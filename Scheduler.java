import java.util.*;

public class Scheduler extends Thread
{
    private Vector queue0;
    private Vector queue1;
    private Vector queue2;
    private int timeSlice;
    private static final int DEFAULT_TIME_SLICE = 500;

    // New data added to p161 
    private boolean[] tids; // Indicate which ids have been used
    private static final int DEFAULT_MAX_THREADS = 10000;

    // A new feature added to p161 
    // Allocate an ID array, each element indicating if that id has been used
    private int nextId = 0;
    private void initTid( int maxThreads ) {
        tids = new boolean[maxThreads];
        for ( int i = 0; i < maxThreads; i++ )
            tids[i] = false;
    }

    // A new feature added to p161 
    // Search an available thread ID and provide a new thread with this ID
    private int getNewTid( ) {
        for ( int i = 0; i < tids.length; i++ ) {
            int tentative = ( nextId + i ) % tids.length;
            if ( tids[tentative] == false ) {
                tids[tentative] = true;
                nextId = ( tentative + 1 ) % tids.length;
                return tentative;
            }
        }
        return -1;
    }

    // A new feature added to p161 
    // Return the thread ID and set the corresponding tids element to be unused
    private boolean returnTid( int tid ) {
        if ( tid >= 0 && tid < tids.length && tids[tid] == true ) {
            tids[tid] = false;
            return true;
        }
        return false;
    }

    // A new feature added to p161 
    // Retrieve the current thread's TCB from any of the three queues
    public TCB getMyTcb( ) {
        Thread myThread = Thread.currentThread( ); // Get my thread object
        synchronized( queue2 ) {
            for ( int i = 0; i < queue2.size( ); i++ ) {
                TCB tcb = ( TCB )queue2.elementAt( i );
                Thread thread = tcb.getThread( );
                if ( thread == myThread ) // if this is my TCB, return it
                    return tcb;
            }
        }
        synchronized( queue1 ) {
            for ( int i = 0; i < queue1.size( ); i++ ) {
                TCB tcb = ( TCB )queue1.elementAt( i );
                Thread thread = tcb.getThread( );
                if ( thread == myThread ) // if this is my TCB, return it
                    return tcb;
            }
        }

        synchronized( queue0 ) {
            for ( int i = 0; i < queue0.size( ); i++ ) {
                TCB tcb = ( TCB )queue0.elementAt( i );
                Thread thread = tcb.getThread( );
                if ( thread == myThread ) // if this is my TCB, return it
                    return tcb;
            }
        }

        return null;
    }

    // A new feature added to p161
    // Return the maximal number of threads to be spawned in the system
    public int getMaxThreads( ) {
        return tids.length;
    }

    public Scheduler( ) {
        timeSlice = DEFAULT_TIME_SLICE;
        queue0 = new Vector( );
        queue1 = new Vector( );
        queue2 = new Vector( );
        initTid( DEFAULT_MAX_THREADS );
    }

    public Scheduler( int quantum ) {
        timeSlice = quantum;
        queue0 = new Vector( );
        queue1 = new Vector( );
        queue2 = new Vector( );
        initTid( DEFAULT_MAX_THREADS );
    }

    // A new feature added to p161 
    // A constructor to receive the max number of threads to be spawned
    public Scheduler( int quantum, int maxThreads ) {
        timeSlice = quantum;
        queue0 = new Vector( );
        queue1 = new Vector( );
        queue2 = new Vector( );
        initTid( maxThreads );
    }

    private void schedulerSleep( ) {
        try {
            Thread.sleep( timeSlice );
        } catch ( InterruptedException e ) {
        }
    }

    // A modified addThread of p161 example
    public TCB addThread( Thread t ) {
        TCB parentTcb = getMyTcb( ); // get my TCB and find my TID
        int pid = ( parentTcb != null ) ? parentTcb.getTid( ) : -1;
        int tid = getNewTid( ); // get a new TID
        if ( tid == -1)
            return null;
        TCB tcb = new TCB( t, tid, pid ); // create a new TCB
        queue0.add( tcb );
        return tcb;
    }

    // A new feature added to p161
    // Removing the TCB of a terminating thread
    public boolean deleteThread( ) {
        TCB tcb = getMyTcb( );
        if ( tcb!= null )
            return tcb.setTerminated( );
        else
            return false;
    }

    public void sleepThread( int milliseconds ) {
        try {
            sleep( milliseconds );
        } catch ( InterruptedException e ) { }
    }


    private void runQ0(){

        Thread current = null;
        while (queue0.size( ) > 0){
            try{

                TCB currentTCB = (TCB)queue0.firstElement( );             // get next tcb in queue0

                if ( currentTCB.getTerminated( ) == true ) {           //if it's done, remove it
                    queue0.remove( currentTCB );
                    returnTid(currentTCB.getTid());
                    continue;
                }

                current = currentTCB.getThread( );    //get the actual thread, and either resume or start it
                if ( current != null ) {
                    if ( current.isAlive( ) ){
                        current.resume();
                    }
                    else {
                        // Spawn must be controlled by Scheduler
                        // Scheduler must start a new thread
                        current.start( );
                    }
                }
                schedulerSleep( );                  // let it run for 1/2 quantum

                synchronized ( queue0 ) {                                //post execution cleanup
                    if ( current != null && current.isAlive( ) )
                        current.suspend();                             //suspend it
                    queue0.remove( currentTCB );                // remove from Q0
                    queue1.add( currentTCB );                   // add to Q1
                }
            } catch ( NullPointerException e3 ) { };



        }


    }





    private void runQ1(){

        Thread current = null;
        while (queue1.size( ) > 0){                      //loop until queue1 is empty
            try{

                TCB currentTCB = (TCB)queue1.firstElement( );             // get next tcb in queue1

                if ( currentTCB.getTerminated( ) == true ) {           //if it's done, remove it

                    queue1.remove( currentTCB );
                    returnTid(currentTCB.getTid());

                    continue;
                }

                current = currentTCB.getThread( );    //get the actual thread, and resume it
                if ( current != null ) {
                    if ( current.isAlive( ) ){

                        current.resume();

                    }

                }


                schedulerSleep( );             // let it run for at least 1/2 quantum

                current.suspend();             // Let queue0 pre-empt
                runQ0();
                current.resume();

                schedulerSleep( );             // let it run for another 1/2 quantum


                synchronized ( queue1 ) {                                  //post execution cleanup
                    if ( current != null && current.isAlive( ) )
                        current.suspend();                             //suspend it
                    queue1.remove( currentTCB );                // remove from Q1
                    queue2.add( currentTCB );                   // add to Q2
                }
            } catch ( NullPointerException e3 ) { };



        }



    }



    private void runQ2(){
        Thread current = null;
        if (queue2.size( ) > 0){  //looping handled by run()
            try{

                TCB currentTCB = (TCB)queue2.firstElement( );             // get next tcb in queue2

                if ( currentTCB.getTerminated( ) == true ) {           //if it's done, remove it

                    queue2.remove( currentTCB );
                    returnTid(currentTCB.getTid());

                    return;
                }

                current = currentTCB.getThread( );    //get the actual thread, and resume it
                if ( current != null ) {
                    if ( current.isAlive( ) ){
                        current.resume();
                    }

                }

                schedulerSleep( );           // let it run for at least 1/2 quantum

                for (int i = 3; i > 0; i--){         // run for 3 more 1/2 quantums,
                                                     // allowing Q1 to preempt before each
                    current.suspend();
                    runQ1();
                    current.resume();

                    schedulerSleep( );
                }

                synchronized ( queue2 ) {                           //post execution cleanup
                    if ( current != null && current.isAlive( ) )
                        current.suspend();                             //suspend it
                    queue2.remove( currentTCB );                // put it in the back
                    queue2.add( currentTCB );
                }
            } catch ( NullPointerException e3 ) { };



        }



    }







    // A modified run of p161
    public void run( ) {

        while ( true ) {
            runQ0();
            runQ1();
            runQ2();


        }
    }
}
