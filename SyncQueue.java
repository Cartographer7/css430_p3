/**
 * Created with IntelliJ IDEA.
 * User: callar
 * Date: 5/11/15
 * Time: 3:53 PM
 * To change this template use File | Settings | File Templates.
 */


import java.util.*;

public class SyncQueue {
    private QueueNode[] queue;
    private final boolean DEBUG = false;
    private final int DEFAULT_COND_MAX = 10;
    private int condMax;
    private int childID;
    private final int NO_PID = -1;

    private void initQueue(int condMax) {           // initalizes the queue to null
        queue = new QueueNode[condMax];
        for (int i = 0; i < condMax; i++){
            queue[i] = null;
        }

     /* compiled code */ }

    public SyncQueue() {
        condMax = DEFAULT_COND_MAX;
        initQueue(condMax);
                                                    //what should I be setting the queue size to?
                                                    //based on the usage:
                                                    //"SyncQueue waitQueue = new SyncQueue( scheduler.getMaxThreads( ) );"
                                                    // I'm going to set it to condMax

    }

    public SyncQueue(int condMax) {
        this.condMax = condMax;
        initQueue(this.condMax);
    }

    int enqueueAndSleep(int condition) {
        if (DEBUG)
            SysLib.cerr("Enqueue called on condition " + condition + " by " + Scheduler.currentThread().getName() + "\n");

        if (condition == 1){
            int i = 0;        // dummy state trap statement
        }


        if (queue[condition] == null){           // create new node if necessary
            queue[condition] = new QueueNode(condition);
        }

        queue[condition].SleepSpin();            // start spinning that condition

        return childID;
    }

    void dequeueAndWakeup(int condition, int childID) {
        queue[condition].wakeup();
        this.childID = childID;
    }

    void dequeueAndWakeup(int condition) {
        queue[condition].wakeup();
        childID = 0;
    }


    private class QueueNode {
        private int sleeping = 0;   // 0 means sleeping, more than 0 is the number of
        private int condition;                            // exits since the current SleepSpin started running
        public QueueNode(int condition) {
            this.condition = condition;

        }


        //takes the first thread to call it and spin it until wakeup() is called.
        //any other threads which try to call it are blocked by the synchronized
        //keyword until the current thread finishes.

        //this functionality could probably be reproduced using Thread.currentThread()
        //along with a vector inside QueueNode

        public synchronized void SleepSpin(){
            if (DEBUG)
                SysLib.cerr("Spinning " + condition + " start\n");

            while (sleeping < 1){
                try{
                    Thread.currentThread().sleep(500);                //better way to do this?
                } catch (InterruptedException e){
                    SysLib.cerr( e.toString( ) + "\n" );
                }
            }
            if (DEBUG)
                SysLib.cerr("Spinning " + condition + " end\n");

            sleeping--;
            if (condition == 1){
                int i = 0;        // dummy state trap statement
            }
        }


        public void wakeup(){           // breaks sleepspin out of its loop.
                                        // if more than one wakeup is called
                                        // in one spin cycle, they accumulate
                                        // and none are lost.
            if (DEBUG)
                SysLib.cerr("Wakeup Called on condition " + condition + "\n");

            sleeping++;
        }


    }
}
